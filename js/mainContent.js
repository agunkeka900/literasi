let MCTglEvent = null;
let MCLoc = null;
function mainContent(link) {
    if(link === false){
        document.getElementById("titleMainContent").innerHTML       = dataMainContent[1].title;
        document.getElementById("fillMainContent").innerHTML        = dataMainContent[1].fill;
        document.getElementById("maps").style.display   = "none";
        document.getElementById('MCKet').style.display = "none";
    }
    else if(link.includes('/') === false){
        let found = false;
        let index;
        for(let i = 0; i < dataMainContent.length; i++){
            if(dataMainContent[i].page === link && found === false){
                found = true;
                index = i;
            }
            else if(dataMainContent[i].page !== link && found === false){
                index = 0;
            }
        }
        document.getElementById('MCKet').style.display = "none";
        document.getElementById("slideshow").style.display          = "block";
        document.getElementById("eventImgContainer").style.display  = "none";
        document.getElementById("titleMainContent").style.display   = "none";
        document.getElementById("fillMainContent").style.display    = "none";
        document.getElementById("maps").style.display    = "none";
        document.getElementById("titleMainContent").innerHTML       = dataMainContent[index].title;
        document.getElementById("fillMainContent").innerHTML        = dataMainContent[index].fill;
        setTimeout(function () {
            document.getElementById("titleMainContent").style.display   = "block";
            document.getElementById("fillMainContent").style.display    = "block";
        }, 100);
    }
    else if(link.includes('/')){
        let found = false;
        let index;
        let page = link.split('/');
        for(let i = 0; i < dataEvent.length; i++){
            if(dataEvent[i].link === page[1] && found === false){
                found = true;
                index = i;
            }
        }
        showTopMenu(page[0]);
        document.getElementById('MCKet').style.display = "none";
        document.getElementById('MCTglEvent').innerText = dataEvent[index].start + ' - ' + dataEvent[index].end;
        document.getElementById('MCLoc').innerText = dataEvent[index].city;
        document.getElementById("eventImg").setAttribute("src", "img/" + dataEvent[index].photo);
        document.getElementById("slideshow").style.display          = "none";
        document.getElementById("eventImgContainer").style.display  = "none";
        document.getElementById("titleMainContent").style.display   = "none";
        document.getElementById("fillMainContent").style.display    = "none";
        document.getElementById("maps").style.display               = "none";
        document.getElementById("titleMainContent").innerHTML       = dataEvent[index].title;
        document.getElementById("fillMainContent").innerHTML        = dataEvent[index].desc;
        document.getElementById("maps").setAttribute('src', dataEvent[index].maps);
        setTimeout(function () {
            document.getElementById("titleMainContent").style.display   = "block";
            document.getElementById("fillMainContent").style.display    = "block";
            document.getElementById("maps").style.display               = "block";
            document.getElementById("eventImgContainer").style.display  = "block";
            document.getElementById('MCKet').style.display              = "block";
        }, 100);
    }
}