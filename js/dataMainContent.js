let dataMainContent = [
    {
        "page"  :   "404",
        "title" :   "404 Not Found!",
        "fill"  :   "Mohon maaf, halaman tidak tersedia!"

    },
    {
        "page"  :   "Home",
        "title" :   "BALI",
        "fill"  :   "Bali merupakan sebuah provinsi di Indonesia , Selain terdiri dari Pulau Bali, wilayah Provinsi Bali " +
        "juga terdiri dari pulau-pulau yang lebih kecil di sekitarnya, yaitu Pulau Nusa Penida, Pulau Nusa Lembongan, Pulau " +
        "Nusa Ceningan, Pulau Serangan, dan Pulau Menjangan. <br><br> Bali  beribukota di Denpasar. Bali  merupakan sebuah " +
        "pulau kecil cantik yang indah dengan sebuah panorama yang mempesona . Bali merupakan sebuah ikon pariwisata di " +
        "Indonesia , bahkan di Asia Tenggara .Bali sudah tidak asing lagi bagi para wisatawan baik di dalam negeri maupun " +
        "luar negeri , karena menyimpan begitu banyak adat tradisi dan budaya warisan nenek moyang yang masih dilestarikan " +
        "sampai saat ini , hal itu merupakan sebuah daya tarik tersendiri bagi para wisatawan . Bali juga dikenal dengan " +
        "sebutan pulau dewata dan pulau seribu pura , selain destinasi wisata yang indah , bali juga memiliki nilai-nilai " +
        "kesakralan yang masih dilestarikan hingga saat ini yang sangat dihormati oleh para warga Bali , seperti persembahyangan " +
        "secara berkala ke pura dan juga upacara-upacara yadnya yang sering kita lihat dilakukan di desa-desa ."
    },
    {
        "page"  :   "Denpasar",
        "title" :   "DENPASAR",
        "fill"  :   "Kota Denpasar adalah ibu kota Provinsi Bali, Indonesia. Denpasar merupakan kota terbesar di Kepulauan " +
        "Nusa Tenggara dan kota terbesar kedua di wilayah Indonesia Timur setelah Makassar. Pertumbuhan industri pariwisata " +
        "di Pulau Bali mendorong Kota Denpasar menjadi pusat kegiatan bisnis, dan menempatkan kota ini sebagai daerah yang " +
        "memiliki pendapatan per kapita dan pertumbuhan tinggi di Provinsi Bali.  <br><br> Nama Denpasar berasal dari kata " +
        "\"den\" (utara) dan \"pasar\" sehingga secara keseluruhan bermakna \"Utara Pasar\". Denpasar pada mulanya adalah " +
        "sebuah taman. Namun taman tersebut tidak seperti taman pada umumnya, karena merupakan taman kesayangan dari Raja " +
        "badung pada waktu itu, Kyai Jambe Ksatrya. Pada waktu itu, Kyai Jambe Ksatrya tinggal di Puri Jambe Ksatrya, yang " +
        "kini menjadi Pasar Satria. Taman ini unik, karena dilengkapi dengan tempat untuk bermain adu ayam. Hobi Kyai Jambe " +
        "Ksatrya adalah bermain adu ayam, oleh karena itu tidak jarang sang raja mengundang raja-raja lainnya di Bali untuk " +
        "bermain adu ayam di taman tersebut <br><br>Nama Denpasar berasal dari kata \"den\" (utara) dan \"pasar\" sehingga " +
        "secara keseluruhan bermakna \"Utara Pasar\". Denpasar pada mulanya adalah sebuah taman. Namun taman tersebut tidak " +
        "seperti taman pada umumnya, karena merupakan taman kesayangan dari Raja badung pada waktu itu, Kyai Jambe Ksatrya. " +
        "Pada waktu itu, Kyai Jambe Ksatrya tinggal di Puri Jambe Ksatrya, yang kini menjadi Pasar Satria. Taman ini unik," +
        " karena dilengkapi dengan tempat untuk bermain adu ayam. Hobi Kyai Jambe Ksatrya adalah bermain adu ayam, oleh karena " +
        "itu tidak jarang sang raja mengundang raja-raja lainnya di Bali untuk bermain adu ayam di taman tersebut"
    },
    {
        "page"  :   "Badung",
        "title" :   "BADUNG",
        "fill"  :   "Kabupaten Badung adalah sebuah kabupaten yang terletak di provinsi Bali, Indonesia. Daerah ini yang " +
        "juga meliputi Kuta dan Nusa Dua adalah sebuah objek wisata yang terkenal <br><br>Kabupaten Badung berbatasan dengan " +
        "Kabupaten Buleleng di sebelah utara, Kabupaten Tabanan di barat dan Kabupaten Bangli, Gianyar serta kota Denpasar " +
        "di sebelah timur. <br><br>Kabupaten Badung dulunya bernama Nambangan sebelum diganti oleh I Gusti Ngurah Made " +
        "Pemecutan pada akhir abad ke-18. Dengan memiliki keris dan cemeti pusaka Dia dapat menundukkan Mengwi dan Jembrana " +
        "hingga tahun 1810, di mana Dia akhirnya diganti oleh 2 orang raja berikutnya. Kematian Dia seolah olah sudah diatur " +
        "oleh penerusnya, barangkali saudaranya, Raja Kesiman yang memerintah dengan mencapai puncaknya tahun 1829-1863. " +
        "Ia dapat dipengaruhi oleh kekuatan dari luar Bali dan menggantungkan harapan kepada Pemerintah Belanda pada saat itu"
    },
    {
        "page"  :   "Tabanan",
        "title" :   "TABANAN",
        "fill"  :   "Kabupaten Tabanan adalah sebuah kabupaten di provinsi Bali, Indonesia, terletak sekitar 35 km di sebelah " +
        "barat kota Denpasar. Tabanan berbatasan dengan Kabupaten Buleleng di sebelah utara, Kabupaten Badung di timur, Samudra " +
        "Indonesiadi selatan dan Kabupaten Jembrana di barat. Luas Kabupaten Tabanan adalah 839,33 km² <br><br>" +
        "Kabupaten Tabanan merupakan lumbung padi di pulau Bali , karena memiliki wilayah sawah yang lebih luas dibanding " +
        "wilayah-wilayah lain , selain itu wilayah Tabanan juga memiliki ciri berupa memiliki tempat-tempat wisata alam " +
        "yang bisa dibilang lebih lestari dan belum banyak terjamah oleh para wisatawan <br><br>" +
        "Kabupaten tabanan memiliki objek-objek wisata alam seperti Tanah lot dan Jatiluwih yang merupakan salah satu penghasil " +
        "pundi-pundi rupiah bagi Kabupaten tabanan"
    },
    {
        "page"  :   "Jembrana",
        "title" :   "JEMBRANA",
        "fill"  :   "Kabupaten Jembrana adalah sebuah kabupaten yang terletak di ujung barat pulau Bali, Indonesia. Ibukotanya " +
        "berada di Negara. Kabupaten Jembrana berbatasan dengan Kabupaten Tabanan di timur, Kabupaten Buleleng di Utara, " +
        "Selat Bali di barat dan Samudera Hindia di selatan. <br><br>" +
        "Berdasarkan bukti-bukti arkeologis dapat diinterprestasikan bahwa munculnya pemukiman di Jembrana sudah sejak 6000 " +
        "tahun yang lalu. Dari perspektif semiotik, asal-usul nama tempat atau kawasan mengacu nama-nama fauna dan flora. " +
        "Munculnya nama Jembrana berasal dari kawasan hutan belantara (Jimbar-Wana) yang dihuni raja ular (Naga-Raja). " +
        "Sifat-sifat mitologis dari penyebutan nama-nama tempat telah mentradisi melalui cerita turun-temurun di kalangan " +
        "penduduk. <br><br>" +
        "Berdasarkan cerita rakyat dan tradisi lisan (folklore) yang muncul telah memberi inspirasi di kalangan pembangun " +
        "lembaga kekuasaan tradisional (raja dan kerajaan)Raja dan pengikutnya yaitu rakyat yang berasal dari etnik Bali " +
        "Hindu maupun dari etnik non Bali yang beragama Islam telah membangun kraton sebagai pusat pemerintahan yang diberi " +
        "nama Puri Gede Jembrana pada awal abad XVII oleh I Gusti Made Yasa (penguasa Brangbang). Raja I yang memerintah " +
        "di kraton (Puri) Gede Agung Jembrana adalah I Gusti Ngurah Jembrana. Selain kraton, diberikan pula rakyat pengikut " +
        "(wadwa),busana kerajaan yang dilengkapi barang-barang pusaka berupa tombak dan tulup. Demikian pula keris pusaka " +
        "yang diberi nama \"Ki Tatas\" untuk memperbesar kewibawaan kerajaan. Tercatat bahwa ada tiga orang raja yang berkuasa " +
        "di pusat pemerintahan yaitu di Kraton (Puri) Agung Jembrana."
    },
    {
        "page"  :   "Gianyar",
        "title" :   "GIANYAR",
        "fill"  :   "Kabupaten Gianyar adalah sebuah kabupaten di provinsi Bali, Indonesia. Daerah ini merupakan pusat budaya " +
        "ukiran di Bali. Gianyar berbatasan dengan Kota Denpasar di barat daya, Kabupaten Badung di barat, Kabupaten Bangli " +
        "di timur dan Kabupaten Klungkung di tenggara <br><br>" +
        "Sejarah Kota Gianyar ditetapkan dengan Peraturan Daerah Kabupaten Gianyar No.9 tahun 2004 tanggal 2 April 2004 " +
        "tentang Hari jadi Kota Gianyar. <br><br>" +
        "Sejarah dua seperempat abad lebih, tepatnya 244 tahun yang lalu, 19 April 1771, ketika Gianyar dipilih menjadi " +
        "nama sebuah keraton, Puri Agung yaitu Istana Raja (Anak Agung) oleh Ida Dewa Manggis Sakti maka sebuah kerajaan " +
        "yang berdaulat dan otonom telah lahir serta ikut pentas dalam percaturan kekuasaan kerajaan-kerajaan di Bali. <br><br>" +
        "Sesungguhnya berfungsinya sebuah keraton, yaitu Puri Agung Gianyar yang telah ditentukan oleh syarat sekala niskala " +
        "yang jatuh pada tanggal 19 April 1771 adalah tonggak sejarah yang telah dibangun oleh raja (Ida Anak Agung) Gianyar I, " +
        "Ida Dewata Manggis Sakti memberikan syarat kepada kita bahwa proses menjadi dan ada itu bisa ditarik ke belakang " +
        "   (masa sebelumnya) atau ditarik ke depan (masa sesudahnya)."
    },
    {
        "page"  :   "Klungkung",
        "title" :   "KLUNGKUNG",
        "fill"  :   "Kabupaten Klungkung adalah kabupaten terkecil di provinsi Bali, Indonesia. Ibukotanya berada di Semarapura. " +
        "Klungkung berbatasan dengan Kabupaten Bangli di sebelah utara, Kabupaten Karangasem di timur, Kabupaten Gianyar " +
        "di barat dan dengan Samudra Hindia di sebelah selatan. <br><br>" +
        "Sepertiga wilayah Kabupaten Klungkung (112,16 km²) terletak di antara pulau Bali dan dua pertiganya (202,84 km²) " +
        "lagi merupakan kepulauan, yaitu Nusa Penida, Nusa Lembongan dan Nusa Ceningan. <br><br>" +
        "Pada zaman kerajaan, Klungkung menjadi pusat pemerintahan raja-raja Bali. Ida I Dewa Agung Jambe adalah Pendiri " +
        "Kerajaan Klungkung tahun 1686 dan merupakan penerus Dinasti Gelgel. Kerajaan Gelgel pada waktu itu merupakan pusat " +
        "kerajaan di Bali dan masa keemasan kerajaan ini tercipta pada masa pemerintahan Dalem Watu Renggong. Raja Klungkung " +
        "adalah pewaris langsung dan keturunan dari Dinasti Kresna Kepakisan. Oleh karenanya, sejarah Klungkung berhubungan " +
        "erat dengan raja-raja yang memerintah di Samprangan dan Gelgel. <br><br>" +
        "Pada tahun 1650, terjadi pemberontakan oleh seorang Perdana Menteri Kerajaan bernama I Gusti Agung Maruti yang " +
        "menyebabkan runtuhnya Kerajaan Gelgel yang pada saat itu diperintah Dalem Dimade. Gusti Agung Maruti mengambil " +
        "alih Kerajaan tersebut dari tangan Dalem Dimade raja terakhir yang memerintah kerajaan Gelgel. Pada waktu itu Dalem " +
        "Dimade menyelamatkan diri dengan mengungsi ke Desa Guliang di wilayah Kerajaan Bangli. Salah seorang Putranya, " +
        "Ida I Dewa Agung Jambe, kemudian berhasil merebut kembali kerajaan Gelgel dari cengkraman I Gusti Agung Maruti " +
        "pada tahun 1686 M. Sejak itu Gelgel tidak lagi sebagai tempat kerajaan. Di daerah utara dari Gelgel, yang kemudian " +
        "dinamai Klungkung, disitulah kemudian Ida I Dewa Agung Jambe mendirikan Istana tempat tinggal. Istana ini kemudian " +
        "dinamakan Semarapura atau Semarajaya. Sejak itu gelar \"Dalem\" tidak lagi dipergunakan bagi raja- raja yang memerintah " +
        "di Kerajaan Klungkung. Gelar yang disandang secara turun–temurun oleh raja – raja Klungkung disebut \"Dewa Agung\"."
    },
    {
        "page"  :   "Bangli",
        "title" :   "BANGLI",
        "fill"  :   "Kabupaten Bangli adalah sebuah kabupaten yang terletak di provinsi Bali, Indonesia. Kabupaten Bangli " +
        "adalah satu-satunya kabupaten di Bali yang tidak memiliki wilayah laut (terkurung daratan). Bangli berbatasan " +
        "dengan Kabupaten Buleleng di sebelah utara, kabupaten Klungkung dan Karangasem di timur, dan kabupaten Klungkung, " +
        "Gianyar di selatan serta Badung dan Gianyar di sebelah barat. <br><br>" +
        "Pada tahun 2004, Bangli mempunyai luas sebesar 520,81 km². Penduduknya berjumlah 197.210 jiwa. Objek wisata di " +
        "daerah ini antara lain adalah danau Batur. Ibu kotanya berada di Bangli. <br><br>" +
        "Menurut Prasasti Pura Kehen yang tersimpan di Pura Kehen, diceritakan bahwa pada abad ke-11 di Desa Bangli berkembang " +
        "wabah penyakit yang disebut kegeringan yang menyebabkan banyak penduduk meninggal. Penduduk lainnya yang masih " +
        "hidup dan sehat menjadi ketakutan setengah mati, sehinnga mereka berbondong-bondong meninggalkan desa guna menghindari " +
        "wabah tersebut. Akibatnya Desa Bangli menjadi kosong karena tidak ada seorangpun yang berani tinggal di sana. <br><br>" +
        "Raja Ida Bhatara Guru Sri Adikunti Ketana yang bertahta ketika itu berusaha mengatasi wabah tersebut. Setelah keadaan " +
        "pulih kembali, sang raja yang bertahta pada tahun Caka 1126, tanggal 10 Tahun Paro Terang, Hari Pasaran Maula, " +
        "Kliwon, Chandra (senin), Wuku Klurut tepatnya pada tanggal 10 Mei 1204, memerintahkan kepada putra-putrinya yang " +
        "bernama Dhana Dewi Ketu agar mengajak penduduk kembali ke Desa Bangli guna bersama-sama membangun dan memperbaiki " +
        "rumahnya masing-masing sekaligus menyelenggarakan upacara/yadnya pada bulan Kasa, Karo, Katiga, Kapat, Kalima, " +
        "Kalima, Kanem, Kapitu, Kaulu, Kasanga, Kadasa, Yjahstha dan Sadha. Disamping itu, raja juga memerintahkan kepada " +
        "seluruh penduduk agar menambah keturunan di wilayah Pura Loka Serana di Desa Bangli dan mengijinkan membabat hutan " +
        "untuk membuat sawah dan saluran air. Untuk itu pada setiap upacara besar penduduk yang ada di Desa Bangli harus " +
        "melakukan persembahyangan. <br><br>" +
        "Pada saat itu juga, tanggal 10 Mei 1204, Raja Idha Bhatara Guru Sri Adikunti Katana mengucapkan pemastu yaitu: <br><br>" +
        "<i>“Barang siapa yang tidak tunduk dan melanggar perintah, semoga orang itu disambar petir tanpa hujan atau mendadak " +
        "jatuh dari titian tanpa sebab, mata buta tanpa catok, setelah mati arwahnya disiksa oleh Yamabala, dilempar dari " +
        "langit turun jatuh ke dalam api neraka”.</i><br><br>" +
        "Bertitik tolak dari titah-titah Sang Raja yang dikeluarkan pada tanggal 10 Mei 1204, maka pada tanggal tersebut " +
        "ditetapkan sebagai hari lahirnya Kota Bangli."
    },
    {
        "page"  :   "Karangasem",
        "title" :   "KARANGASEM",
        "fill"  :   "Kabupaten Karangasem atau Karang Asem adalah sebuah kabupaten yang terletak di provinsi Bali, Indonesia. " +
        "Ibukotanya berada di Amlapura. Memiliki dua Pelabuhan yakni Padang Bai dan Tanah Ampo. Di kabupaten ini terletak " +
        "pura terbesar di Bali, yaitu Pura Besakih. <br><br>" +
        "Sejarah Karangasem tidak lepas dengan sejarah berdirinya Kerajaan Karangasem. Nama Karangasem sebenarnya berasal " +
        "dari kata Karang Semadi. Beberapa catatan yang memuat asal-muasal nama karangasem adalah seperti yang diungkapkan " +
        "dalam Prasasti Sading C yang terdapat di Geria Mandara, Munggu, Badung. Lebih lanjut diungkapkan bahwa Gunung Lempuyang " +
        "di timur laut Amlapura, pada mulanya bernama Adri Karang yang berarti Gunung Karang. <br><br>" +
        "Dalam penelitian sejarah keberadaan pura, Lempuyang dihubungkan dengan kata lampu yang artinya terpilih, dan Hyang " +
        "yang berarti Tuhan (Bathara Guru, Hyang Parameswara). Di Adri Karang inilah Hyang Agnijaya membuat Pura Lempuyang " +
        "Luhur sebagai tempat bersemadi (Karang Semadi). Lambat laun nama Karang Semadi ini berubah menjadi Karangasem"
    },
    {
        "page"  :   "Buleleng",
        "title" :   "BULELENG",
        "fill"  :   "Kabupaten Buleleng adalah sebuah kabupaten di provinsi Bali, Indonesia. Ibu kotanya ialah Singaraja. " +
        "Buleleng berbatasan dengan Laut Jawa di sebelah utara, Selat Bali di sebelah barat, Kabupaten Karangasem di sebelah " +
        "timur dan Kabupaten Jembrana, Bangli, Tabanan serta Badung di sebelah selatan. <br><br>" +
        "Panjang ruas pantai Kabupaten Buleleng sekitar 144 km, 19 km-nya melewati kecamatan Tejakula. Selain sebagai penghasil " +
        "pertanian terbesar di Bali (terkenal dengan produksi salak bali dan jeruk keprok Tejakula), Kabupaten Buleleng " +
        "juga memiliki objek pariwisata yang cukup banyak seperti pantai Lovina, pura Pulaki, Air Sanih dan tentunya kota " +
        "Singaraja sendiri <br><br>" +
        "Kabupaten paling utara pulau bali ini mempunyai objek wisata seperti pantai lovina yang indah, selain itu kabupaten " +
        "buleleng juga memiliki adat dan budaya yang sangat kental , hal ini karena daerah ini tidak banyak dipengaruhi " +
        "oleh adanya wisatawan , seperti di wilayah bali yang lain seperti badung ataupun denpasar <br><br>"
    },
];