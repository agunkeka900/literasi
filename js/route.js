let previousLocation;
function route() {
    let location = window.location.href;
    let link     = location.split('#');
    if(link.length === 2){
        let link2 = link[1].split('/');
        showSinglePage(link[1]);
        showEventR(link2[0]);
        mainContent(link[1]);
        showTopMenu(link2[0]);
        changeTitleRightMenu(link2[0]);
    }
    else if(location.includes('#') === false){
        mainContent(false);
        showEventR(false);
        showTopMenu('Home');
        changeTitleRightMenu('Home');
    }
    else{
        showSinglePage(link[1]);
        mainContent(link[1]);
        showTopMenu(link[1]);
        showEventR(link[1]);
        changeTitleRightMenu(link[1]);
    }
    changeSlideShow(link[1]);
}
function linkTo(link) {
    previousLocation = window.location.href.split('#')[1];
    location.replace("index.html#"+link);
    showSinglePage(link);
    showEventR(link);
    changeSlideShow(link);
    mainContent(link);
    changeTitleRightMenu(link);
}

function showSinglePage(show) {
    if(show === 'about'){
        showTopMenu(false);
        document.getElementById('mainContent').style.display    = "none";
        document.getElementById('rightContent').style.display   = "none";
        document.getElementById('about').style.display          = "block";
        document.getElementById('contact').style.display        = "none";
        document.getElementById('btnAbout').style.display       = "none";
        document.getElementById('btnContact').style.display     = "block";
        document.getElementById('footerMenu').style.cssFloat    = "right";
    }
    else if(show === 'contact'){
        showTopMenu(false);
        document.getElementById('mainContent').style.display    = "none";
        document.getElementById('rightContent').style.display   = "none";
        document.getElementById('about').style.display          = "none";
        document.getElementById('contact').style.display        = "block";
        document.getElementById('btnAbout').style.display       = "block";
        document.getElementById('btnContact').style.display     = "none";
        document.getElementById('footerMenu').style.cssFloat    = "right";
    }
    else{
        document.getElementById('mainContent').style.display    = "block";
        document.getElementById('rightContent').style.display   = "block";
        document.getElementById('about').style.display          = "none";
        document.getElementById('contact').style.display        = "none";
        document.getElementById('btnAbout').style.display       = "block";
        document.getElementById('btnContact').style.display     = "block";
        document.getElementById('footerMenu').style.cssFloat    = "left";
    }
}