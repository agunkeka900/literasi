let searchProcess = false;
function singkatanBulan(nama){
    let bulan   = '';
    if      (nama === '01')    bulan = 'JAN';
    else if (nama === '02')    bulan = 'FEB';
    else if (nama === '03')    bulan = 'MAR';
    else if (nama === '04')    bulan = 'APR';
    else if (nama === '05')    bulan = 'MEI';
    else if (nama === '06')    bulan = 'JUN';
    else if (nama === '07')    bulan = 'JUL';
    else if (nama === '08')    bulan = 'AGU';
    else if (nama === '09')    bulan = 'SEP';
    else if (nama === '10')   bulan = 'OKT';
    else if (nama === '11')   bulan = 'NOV';
    else if (nama === '12')   bulan = 'DES';
    return bulan;
}
function showEventR(grouping) {
    let eventHtml = "";
    if(grouping === false || grouping === 'Home'){
        sortEvent();
    }
    else if(grouping.includes('/') === false){
        for (let i = 0; i < dataEvent.length; i++){
            if(grouping === dataEvent[i].city){
                let waktu   = dataEvent[i].start.split('-');
                let tanggal = waktu[2];
                let bulan   = singkatanBulan(waktu[1]);
                eventHtml +="<div class='eventWrapper' onmousedown='linkTo(`" +dataEvent[i].city+ "/" +dataEvent[i].link+ "`)'  " +
                            "     onmouseenter='hoverEvent("+i+", true)' onmouseleave='hoverEvent("+i+", false)'>               " +
                            "  <div class='eDateW' id='eDateW"+i+"'>                                                            " +
                            "    <div class='eDate' id='eDate"+i+"'>" + tanggal + "</div>                                       " +
                            "    <div class='eMonth' id='eMonth"+i+"'>" + bulan + "</div>                                       " +
                            "  </div>                                                                                           " +
                            "  <div class='eTitle' id='eTitle"+i+"'>" + dataEvent[i].title + "</div>                            " +
                            "  <div class='eDate2' id='eDate2"+i+"'><div class='fa fa-calendar-alt' style='width: 20px'>&nbsp;&nbsp;</div>" + dataEvent[i].start + "</div>                             " +
                            "  <div class='eDate2' id='eDate3"+i+"'><div class='fa fa-map-marker-alt' style='width: 20px'>&nbsp;&nbsp;</div>" + dataEvent[i].city + "</div>                             " +
                            "</div>                                                                                             " +
                            "<div class='eventGaris'></div>                                                                     " ;
            }
        }
        document.getElementById('eventR').innerHTML = eventHtml;
    }
}
function hoverEvent(id, hover) {
    if(hover === true){
        document.getElementById("eDateW" + id).style.borderColor = "#1763B8";
        document.getElementById("eDate" + id).style.background   = "#1763B8";
        document.getElementById("eMonth" + id).style.color = "#1763B8";
        document.getElementById("eTitle" + id).style.color = "#1763B8";
        document.getElementById("eDate2" + id).style.color = "#1763B8";
        document.getElementById("eDate3" + id).style.color = "#1763B8";
    }
    else{
        document.getElementById("eDateW" + id).style.borderColor = "#57585A";
        document.getElementById("eDate" + id).style.background   = "#57585A";
        document.getElementById("eMonth" + id).style.color = "#57585A";
        document.getElementById("eTitle" + id).style.color = "black";
        document.getElementById("eDate2" + id).style.color = "grey";
        document.getElementById("eDate3" + id).style.color = "grey";
    }
}
function showDialog(show) {
    if(show === true){
        document.getElementById('dialog').style.display = "table";
    }
    else{
        document.getElementById('dialog').style.display = "none";
    }
}
function showSearch(show) {
    if(show === true){
        document.getElementById('fillR').style.height = "300px";
        document.getElementById('search').style.display = "block";
        document.getElementById('searchField').focus();
        document.getElementById('btnSearch').setAttribute('onclick', 'showSearch(false)');
        hideItemMenuRight();
    }
    else{
        document.getElementById('fillR').style.height = "360px";
        document.getElementById('search').style.display = "none";
        document.getElementById('btnSearch').setAttribute('onclick', 'showSearch(true)');
        document.getElementById('searchField').value = "";
        searchEvent();

    }
}
function searchEvent() {
    if(document.getElementById('searchField').value === ""){
        let location = window.location.href;
        let link     = location.split('#');
        if(link[1].includes('/')){
            let link2 = link[1].split('/');
            showEventR(link2[0]);
            changeTitleRightMenu(link2[0]);
            showTopMenu(link2[0]);
        }
        else{
            showEventR(link[1]);
            changeTitleRightMenu(link[1]);
            showTopMenu(link[1]);
        }
    }
    else if(searchProcess === false){
        searchProcess = true;
        let text  = document.getElementById('searchField').value.toUpperCase().replace(/\s/g, '');
        let text2 = document.getElementById('searchField').value;
        let index = [];
        let eventHtml = "";
        for(let i=0; i<dataEvent.length; i++){
            if(dataEvent[i].title.toUpperCase().replace(/\s/g, '').includes(text)
            || dataEvent[i].city.toUpperCase().replace(/\s/g, '').includes(text)
            || dataEvent[i].start.toUpperCase().replace(/\s/g, '').includes(text)
            || dataEvent[i].end.toUpperCase().replace(/\s/g, '').includes(text)
            ){
                index.push(i);
            }
        }
        if(index.length === 0){
            eventHtml = "<div class='eventTidakTersedia'>Event \""+text2+"\" tidak tersedia!</div>"
        }
        else{
            for(let a=0; a<index.length; a++){
            let waktu   = dataEvent[index[a]].start.split('-');
            let tanggal = waktu[2];
            let bulan   = singkatanBulan(waktu[1]);
            eventHtml +="<div class='eventWrapper' onmousedown='linkTo(`" +dataEvent[index[a]].city+ "/" +dataEvent[index[a]].link+ "`)'  " +
                "     onmouseenter='hoverEvent("+a+", true)' onmouseleave='hoverEvent("+a+", false)'>               " +
                "  <div class='eDateW' id='eDateW"+a+"'>                                                            " +
                "    <div class='eDate' id='eDate"+a+"'>" + tanggal + "</div>                                       " +
                "    <div class='eMonth' id='eMonth"+a+"'>" + bulan + "</div>                                       " +
                "  </div>                                                                                           " +
                "  <div class='eTitle' id='eTitle"+a+"'>" + dataEvent[index[a]].title + "</div>                            " +
                "  <div class='eDate2' id='eDate2"+a+"'><div class='fa fa-calendar-alt' style='width: 20px'>&nbsp;&nbsp;</div>" + dataEvent[index[a]].start + "</div>                             " +
                "  <div class='eDate2' id='eDate3"+a+"'><div class='fa fa-map-marker-alt' style='width: 20px'>&nbsp;&nbsp;</div>" + dataEvent[index[a]].city + "</div>                             " +
                "</div>                                                                                             " +
                "<div class='eventGaris'></div>   "
        }
        }
        document.getElementById('eventR').innerHTML = eventHtml;
        document.getElementById('titleR').innerHTML = "Cari event" +
            "<font style='font-size: 10px; margin-left: 5px;'>&#9660;</font>";
        searchProcess = false;
    }
}
function sortEvent() {
    let arrayTime = [];
    let arrayDate = [];
    let indexEvent= [];
    let eventHtml = "";
    let now = new Date().getTime();
    for(let a=0; a<dataEvent.length; a++){
        let eventTime = new Date(dataEvent[a].start).getTime();
        if(now < eventTime){
            arrayTime.push(eventTime);
        }
    }
    arrayTime.sort();

    for(let e=0; e<9; e++){
        let time2 = new Date(arrayTime[e]);
        let dateEvent = time2.getDate();
        let montEvent = parseInt(time2.getMonth()) + 1;
        let yearEvent = time2.getFullYear();

        if(String(dateEvent).length === 1){
            dateEvent = '0' + dateEvent;
        }
        if(String(montEvent).length === 1) montEvent = '0' + montEvent;
        let textDate = yearEvent + '-' + montEvent + '-' + dateEvent;
        arrayDate.push(textDate);
    }

    for(let r=0; r<arrayDate.length; r++){
        for(let c=0; c<dataEvent.length; c++){
            if(dataEvent[c].start === arrayDate[r]){
                indexEvent.push(c)
            }
        }
    }

    for (let i = 0; i < 10; i++){
        let waktu   = dataEvent[indexEvent[i]].start.split('-');
        let tanggal = waktu[2];
        let bulan   = singkatanBulan(waktu[1]);

        eventHtml +="<div class='eventWrapper' onmousedown='linkTo(`" +dataEvent[indexEvent[i]].city+ "/" +dataEvent[indexEvent[i]].link+ "`)'  " +
            "     onmouseenter='hoverEvent("+i+", true)' onmouseleave='hoverEvent("+i+", false)'>               " +
            "  <div class='eDateW' id='eDateW"+i+"'>                                                            " +
            "    <div class='eDate' id='eDate"+i+"'>" + tanggal + "</div>                                       " +
            "    <div class='eMonth' id='eMonth"+i+"'>" + bulan + "</div>                                       " +
            "  </div>                                                                                           " +
            "  <div class='eTitle' id='eTitle"+i+"'>" + dataEvent[indexEvent[i]].title + "</div>                            " +
            "  <div class='eDate2' id='eDate2"+i+"'><div class='fa fa-calendar-alt' style='width: 20px'>&nbsp;&nbsp;</div>" + dataEvent[indexEvent[i]].start + "</div>                             " +
            "  <div class='eDate2' id='eDate3"+i+"'><div class='fa fa-map-marker-alt' style='width: 20px'>&nbsp;&nbsp;</div>" + dataEvent[indexEvent[i]].city + "</div>                             " +
            "</div>                                                                                             " +
            "<div class='eventGaris'></div>                                                                     " ;
    }
    document.getElementById('eventR').innerHTML = eventHtml;
}