let activeMenuRight = 0;
function showTopMenu(active) {
    let menuHtml = '';
    for(let i = 0; i < dataMenu.topNavbar.length; i++){
        if(active === dataMenu.topNavbar[i]){
            menuHtml += "<div " +
                "class='menu mActive' " +
                "onclick='showTopMenu(`" +dataMenu.topNavbar[i]+ "`);' " +
                "onmousedown='linkTo(`" +dataMenu.topNavbar[i]+ "`);'>" +
                dataMenu.topNavbar[i] + "</div>"
        }
        else{
            menuHtml += "<div " +
                "class='menu' " +
                "onclick='showTopMenu(`" +dataMenu.topNavbar[i]+ "`);' " +
                "onmousedown='linkTo(`" +dataMenu.topNavbar[i]+ "`);'>" +
                dataMenu.topNavbar[i] + "</div>"
        }
    }
    document.getElementById('menu').innerHTML = menuHtml;
}
function changeTitleRightMenu(link){
    if(link.includes('/') === false){
        let id;
        if(link === 'Home')             id = 0;
        else if(link === 'Denpasar')    id = 1;
        else if(link === 'Badung')      id = 2;
        else if(link === 'Tabanan')     id = 3;
        else if(link === 'Jembrana')    id = 4;
        else if(link === 'Gianyar')     id = 5;
        else if(link === 'Klungkung')   id = 6;
        else if(link === 'Bangli')      id = 7;
        else if(link === 'Karangasem')  id = 8;
        else if(link === 'Buleleng')    id = 9;
        activeMenuRight = id;
        document.getElementById('titleR').innerHTML = dataMenu.event[id].text +
            "<font style='font-size: 10px; margin-left: 5px;'>&#9660;</font>";
    }
}
function showItemMenuRight() {
    let menuHtml = '';
    for(let i = 0; i < dataMenu.event.length; i++){
        if(i !== activeMenuRight)
        menuHtml += "<div class='itemMenuR' onmousedown='chooseMenuRight("+ i +")'>"+ dataMenu.event[i].text +"</div>"
    }
    document.getElementById('titleR').setAttribute('onclick', 'hideItemMenuRight()');
    document.getElementById('itemMenuR').innerHTML = menuHtml;
    document.getElementById('fillR').style.marginTop = '100px';
    showSearch(false);
}
function hideItemMenuRight() {
    document.getElementById('titleR').setAttribute('onclick', 'showItemMenuRight()');
    document.getElementById('itemMenuR').innerHTML = '';
    document.getElementById('fillR').style.marginTop = '0px';
}
function chooseMenuRight(value) {
    this.hideItemMenuRight();
    showEventR(dataMenu.event[value].value);
    activeMenuRight = value;
    console.log('active '+activeMenuRight)
    document.getElementById('titleR').innerHTML = dataMenu.event[value].text +
        "<font style='font-size: 10px; margin-left: 5px;'>&#9660;</font>";
}