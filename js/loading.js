// let allowLoading = true;
function loading() {
    if(localStorage.getItem('allowLoading') === 'true'){
        localStorage.setItem('allowLoading', 'false');
        setTimeout(function () {
            document.getElementById("loading").style.display = "none";
            document.getElementById("header").style.display = "block";
            document.getElementById("body").style.display = "block";
        }, 2000)
    }
    else if(localStorage.getItem('allowLoading') === 'false'){
        document.getElementById("loading").style.display = "none";
        document.getElementById("header").style.display = "block";
        document.getElementById("body").style.display = "block";
    }
    else{
        localStorage.setItem('allowLoading', 'true');
        setTimeout(function () {
            document.getElementById("loading").style.display = "none";
            document.getElementById("header").style.display = "block";
            document.getElementById("body").style.display = "block";
        }, 2000)
    }
    console.log(localStorage.getItem('allowLoading'))
}