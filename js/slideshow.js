let slideshowSequel = 0;
let allowChange = true;
let linkSlideshow = ["Karangasem/TradisiTerteran", "Karangasem/UsabaSumbu", "Karangasem/UsabaDangsil", "Badung/NusaDuaLightFestival", "Gianyar/TradisiMapeed"];
let typeSlideshow = ["terteran.png", "usaba sumbu.jpg", "Usaba-Dangsil-690x450.jpg", "nusa-dua-light-festival.jpg", "mapeed.jpg"];
let sequelNext = slideshowSequel + 1;

function homeSlideshow() {
    let randomEventIndex = [];
    let tempRandomEI = [];
    for(let q=0; q<10; q++){
        let angka = String(parseInt(Math.random() * 44));
        if(angka.length === 1){
            angka = '0' + angka;
        }
        randomEventIndex.push(angka);
    }
    randomEventIndex.sort();
    tempRandomEI.push(randomEventIndex[0]);
    for(let d=0; d<randomEventIndex.length; d++){
        if(tempRandomEI[parseInt(tempRandomEI.length) - 1] !== randomEventIndex[d+1]){
            tempRandomEI.push(randomEventIndex[d+1]);
        }
    }
    randomEventIndex = [];
    for(let r=0; r<5; r++){
        let tempAngka = tempRandomEI[r].split('');
        if(tempAngka[0].includes('0')){
            randomEventIndex.push(tempAngka[1]);
        }
        else {
            randomEventIndex.push(tempRandomEI[r]);
        }
    }
    for(let p=0; p<randomEventIndex.length; p++){
        typeSlideshow.push(dataEvent[randomEventIndex[p]].photo)
    }
    console.log(typeSlideshow);
}

function changeSlideShow(type){
    if(type === 'Home' && type.includes('/') === false){
        typeSlideshow = [];
        linkSlideshow = [];
        for(let i=0; i<dataEvent.length; i++){
            if(dataEvent[i].city === 'Denpasar'){
                typeSlideshow.push(dataEvent[i].photo);
                linkSlideshow.push('Denpasar'+'/'+dataEvent[i].link);
            }
        }
        sequelNextMethod()
    }
    else if(type !== 'Home' && type.includes('/') === false){
        typeSlideshow = [];
        linkSlideshow = [];
        for(let i=0; i<dataEvent.length; i++){
            if(dataEvent[i].city === type){
                typeSlideshow.push(dataEvent[i].photo);
                linkSlideshow.push(type+'/'+dataEvent[i].link);
            }
        }
        sequelNextMethod()
    }
}

function sequelNextMethod() {
    slideshowSequel = 0;
    sequelNext = slideshowSequel + 1;
}

function slideShow(){
    setInterval(function () {
        if(allowChange === true) {
            allowChange = false;
            sequelNext = slideshowSequel + 1;
            if (slideshowSequel === (typeSlideshow.length - 1)) {
                sequelNext = 0;
                slideshowSequel = 0;
            }
            else slideshowSequel += 1;
            document.getElementById("slideshowImg").setAttribute("onclick", "linkTo('"+ linkSlideshow[slideshowSequel] +"')");
            document.getElementById("slideshowImg").style.display = "none";
            document.getElementById("slideshowImg").setAttribute("src", "img/" + typeSlideshow[sequelNext]);
            setTimeout(function () {
                document.getElementById("slideshowImg").style.display = "block";
            }, 1);
            setTimeout(function () {
                document.getElementById("slideshowImg2").setAttribute("src", "img/" + typeSlideshow[slideshowSequel]);
                allowChange = true;
            }, 2000)
        }
    }, 3000)
}
function nextImg(){
    if(allowChange === true){
        allowChange = false;
        let sequelNext = slideshowSequel + 1;
        if(slideshowSequel === (typeSlideshow.length - 1)){
            sequelNext = 0;
            slideshowSequel = 0;
        }
        else slideshowSequel += 1;
        document.getElementById("slideshowImg").style.display = "none";
        document.getElementById("slideshowImg").setAttribute("src", "img/" + typeSlideshow[sequelNext]);
        setTimeout(function(){
            document.getElementById("slideshowImg").style.display = "block";
        }, 100);
        setTimeout(function(){
            document.getElementById("slideshowImg2").setAttribute("src", "img/" + typeSlideshow[slideshowSequel]);
            allowChange = true;
        }, 2000)
    }
}
function prevImg(){
    if(allowChange === true) {
        allowChange = false;
        let sequelPrev = slideshowSequel - 1;
        if (slideshowSequel === 0) {
            sequelPrev = typeSlideshow.length - 1;
            slideshowSequel = typeSlideshow.length - 1;
        }
        else slideshowSequel--;
        document.getElementById("slideshowImg").style.display = "none";
        document.getElementById("slideshowImg").setAttribute("src", "img/" + typeSlideshow[sequelPrev]);
        setTimeout(function(){
            document.getElementById("slideshowImg").style.display = "block";
        }, 100);
        setTimeout(function(){
            document.getElementById("slideshowImg2").setAttribute("src", "img/" + typeSlideshow[slideshowSequel]);
            allowChange = true;
        }, 2000)
    }
}
function showBtnImg(){
    document.getElementById("btnPrev").style.display = "block";
    document.getElementById("btnNext").style.display = "block";
}
function hideBtnImg(){
    document.getElementById("btnPrev").style.display = "none";
    document.getElementById("btnNext").style.display = "none";
}