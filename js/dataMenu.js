let dataMenu =  {
    'topNavbar': [
      'Home',
      'Denpasar',
      'Badung',
      'Tabanan',
      'Jembrana',
      'Gianyar',
      'Klungkung',
      'Bangli',
      'Karangasem',
      'Buleleng',
    ],
    'event': [
        {
            'value': 'Home',
            'text' : 'Event yang akan datang'
        },{
            'value': 'Denpasar',
            'text' : 'Event di Denpasar'
        },{
            'value': 'Badung',
            'text' : 'Event di Badung'
        },{
            'value': 'Tabanan',
            'text' : 'Event di Tabanan'
        },{
            'value': 'Jembrana',
            'text' : 'Event di Jembrana'
        },{
            'value': 'Gianyar',
            'text' : 'Event di Gianyar'
        },{
            'value': 'Klungkung',
            'text' : 'Event di Klungkung'
        },{
            'value': 'Bangli',
            'text' : 'Event di Bangli'
        },{
            'value': 'Karangasem',
            'text' : 'Event di Karangasem'
        },{
            'value': 'Buleleng',
            'text' : 'Event di Buleleng'
        }
    ]
};