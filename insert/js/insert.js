let idEdit, linkUpdate, dataEvent = [];

function toLocalStorage() {
    // localStorage.setItem("dataEvent", JSON.stringify(dataEvent));
    console.log(localStorage.getItem("dataEvent"));
    console.log(JSON.parse(localStorage.getItem("dataEvent")));
}
function splitFoto(foto) {
    foto = foto.split('\\');
    foto = foto[foto.length - 1];
    return foto;
}
function splitEmbed(embed) {
    embed = embed.split('src="');
    embed = embed[1].split('\"')[0];
    return embed;
}
function pilihPhoto() {
    setTimeout(function () {
        if(document.getElementById('fotoEvent').value === ''
            || document.getElementById('filePhoto').innerText === splitFoto(document.getElementById('fotoEvent').value)
        ){
            pilihPhoto();
        }
        else{
            let foto = splitFoto(document.getElementById('fotoEvent').value);
            document.getElementById('filePhoto').innerText = foto;
            document.getElementById('btnPhoto').innerText = "Ubah foto";
        }
    }, 500);
}
function showMainContent(link) {
    let found = false;
    let index;
    let page = link;
    for(let i = 0; i < dataEvent.length; i++){
        if(dataEvent[i].link === page && found === false){
            found = true;
            index = i;
        }
    }
    document.getElementById('mainContent').style.display = "block";
    document.getElementById('MCTglEvent').innerText = dataEvent[index].start + ' - ' + dataEvent[index].end;
    document.getElementById('MCLoc').innerText = dataEvent[index].city;
    document.getElementById("eventImg").setAttribute("src", "img/" + dataEvent[index].photo);
    document.getElementById("titleMainContent").innerHTML       = dataEvent[index].title;
    document.getElementById("fillMainContent").innerHTML        = dataEvent[index].desc;
    document.getElementById("maps").setAttribute('src', dataEvent[index].maps);
}
function readEvent() {
    let event = "<tr>\n" +
        "        <td style=\"width: 399px\"></td>\n" +
        "        <td class=\"titleF\">Nama event</td>\n" +
        "        <td class=\"titleF\">Kota</td>\n" +
        "        <td class=\"titleF\">Mulai</td>\n" +
        "        <td class=\"titleF\">Selesai</td>\n" +
        "        <td class=\"titleF\">Foto</td>\n" +
        "      </tr>";
    for(let i=0; i<dataEvent.length; i++){
        event +=
            "<tr onclick='getEvent("+i+")' onmousedown='showMainContent(`"+ dataEvent[i].link+"`)' class='trHover'>"+
            "<td style='width: 399px'></td> " +
            "<td class='fillF'>"+ dataEvent[i].title +"</td>  " +
            "<td class='fillF'>"+ dataEvent[i].city +"</td> " +
            "<td class='fillF'>"+ dataEvent[i].start +"</td>  " +
            "<td class='fillF'>"+ dataEvent[i].end +"</td>  " +
            "<td class='fillF'>"+ dataEvent[i].photo +"</td>  " +
            "</tr>"
    }
    document.getElementById('event').innerHTML =  event;
    document.getElementById('kotaEvent').value = null;
}
function addEvent() {
    let nama = document.getElementById('namaEvent').value;
    let kota = document.getElementById('kotaEvent').value;
    let mulai = document.getElementById('mulaiEvent').value;
    let selesai = document.getElementById('selesaiEvent').value;
    let desk = document.getElementById('deskripsiEvent').value;
    let maps = document.getElementById('embedEvent').value;
    let photo = document.getElementById('fotoEvent').value;

    if(nama === "" || kota === "" || mulai === "" || desk === "")
        return alert("Nama, kota, tanggal mulai & deskripsi event harus diisi!");

    if(maps.includes('src')) maps = splitEmbed(maps);
    else maps = "Tidak tersedia";

    if(photo.includes('\\')) photo = splitFoto(photo);
    else photo = "Tidak tersedia";

    if(selesai === "") selesai = "Tidak diketahui";

    dataEvent.push({
        "link"  : nama.replace(/\s/g, ''),
        "title" : nama,
        "city"  : kota,
        "start" : mulai,
        "end"   : selesai,
        "photo" : photo,
        "desc"  : desk,
        "maps"  : maps
    });

    readEvent();
    toNull();
    alert('Berhasil menambahkan event!');
    document.getElementById('mulaiEvent').value = null;
}
function getEvent(index){
    idEdit = index;
    linkUpdate = dataEvent[index].link;
    document.getElementById('namaEvent').value = dataEvent[index].title;
    document.getElementById('filePhoto').innerText = dataEvent[index].photo;
    document.getElementById('btnPhoto').innerText = "Ubah foto";
    document.getElementById('kotaEvent').value = dataEvent[index].city;
    document.getElementById('mulaiEvent').value = dataEvent[index].start;
    document.getElementById('selesaiEvent').value = dataEvent[index].end;
    document.getElementById('embedEvent').value = dataEvent[index].maps;
    document.getElementById('deskripsiEvent').value = dataEvent[index].desc;
    document.getElementById('btnTambah').style.display = "none";
    document.getElementById('btnEdit').style.display = "block";
    document.getElementById('btnPreview').style.display = "block";
    document.getElementById('btnDelete').style.display = "block";
}
function deleteEvent() {
    dataEvent.splice(idEdit, 1);
    showPreview(false);
    readEvent();
    toNull();
}
function updateEvent() {
    dataEvent[idEdit].title = document.getElementById('namaEvent').value;
    dataEvent[idEdit].city = document.getElementById('kotaEvent').value;
    dataEvent[idEdit].start = document.getElementById('mulaiEvent').value;
    dataEvent[idEdit].end = document.getElementById('selesaiEvent').value;
    dataEvent[idEdit].photo = document.getElementById('filePhoto').innerText;
    dataEvent[idEdit].desc = document.getElementById('deskripsiEvent').value;
    dataEvent[idEdit].maps = document.getElementById('embedEvent').value;
    document.getElementById('fotoEvent').value = null;

    alert('Berhasil mengedit event!');
    readEvent();
    toNull();
    showMainContent(linkUpdate);
}
function toNull() {
    document.getElementById('namaEvent').value = null;
    document.getElementById('fotoEvent').value = null;
    document.getElementById('kotaEvent').value = null;
    document.getElementById('mulaiEvent').value = null;
    document.getElementById('selesaiEvent').value = null;
    document.getElementById('embedEvent').value = null;
    document.getElementById('deskripsiEvent').value = null;
    document.getElementById('filePhoto').innerText = "";
    document.getElementById('btnPhoto').innerText = "Pilih foto";
    document.getElementById('btnTambah').style.display = "block";
    document.getElementById('btnEdit').style.display = "none";
    document.getElementById('btnDelete').style.display = "none";
    document.getElementById('btnPreview').innerText = "show preview";
    document.getElementById('btnPreview').style.display = "none";
    document.getElementById('btnPreview').setAttribute('onclick', "showPreview(true)");
}
function showPreview(show) {
    if(show === true){
        document.getElementById('preview').style.display = "block";
        document.getElementById('btnPreview').innerText = "hide preview";
        document.getElementById('btnPreview').setAttribute('onclick', "showPreview(false)");
        document.getElementById('atas').style.maxHeight = "25%";
        // document.getElementById('atas').style.display = "none";
    }
    else{
        document.getElementById('preview').style.display = "none";
        document.getElementById('btnPreview').innerText = "show preview";
        document.getElementById('btnPreview').setAttribute('onclick', "showPreview(true)");
        document.getElementById('atas').style.maxHeight = "90%";
        // document.getElementById('atas').style.display = "block";
    }
}

function importJson(){
    document.getElementById('fileJson').click();
}
function handleFiles(files) {
    if (window.FileReader) {
        getAsText(files[0]);
    }
    else alert('Browser yang anda gunakan tidak mendukung import CSV!');
}
function getAsText(fileToRead) {
    let reader = new FileReader();
    reader.onload = this.loadHandler;
    reader.readAsText(fileToRead);
}
function loadHandler(event) {
    document.getElementById('fileJson').value = null;
    let csv = JSON.parse(event.target.result);
    for (let i=0; i<csv.length; i++){
        dataEvent.push({
            "link"  : csv[i].link,
            "title" : csv[i].title,
            "city"  : csv[i].city,
            "start" : csv[i].start,
            "end"   : csv[i].end,
            "photo" : csv[i].photo,
            "desc"  : csv[i].desc,
            "maps"  : csv[i].maps
        });
    }
    console.log(dataEvent);
    readEvent();
    // localStorage.setItem("dataEvent", csv);
    // console.log(JSON.parse(localStorage.getItem("dataEvent")));
}

function exportJson() {
    let textFile = null,
        makeTextFile = function (text) {
            let data = new Blob([text], {type: 'text/plain'});
            if (textFile !== null) {
                window.URL.revokeObjectURL(textFile);
            }
            textFile = window.URL.createObjectURL(data);

            return textFile;
        };

    let link = document.getElementById('downloadlink');
    link.href = makeTextFile(JSON.stringify(dataEvent));
    link.download = "dataEvent.json";

    setTimeout(function () {
        document.getElementById('downloadlink').click();
    }, 100);
}